var app = this;

// Connect to the socket.io network as a peer
app.connectToServer = function () {
    self = this;

    /*
        This socket is used as a bi-directionnal communication
        channel between the server and all peers.
    */
    socket = io.connect('http://' + location.host);

    // Respond to a play request made by a peer
    socket.on('PlayRequest', function (request) {
        console.log('Received PlayRequest...');
        if (localPlayer) {
             self.localPlayer.handlePlayRequest(JSON.parse(request));
        } else {
            console.log('The local player is not initialized... Cannot play the specified.');
        }
    });

    // Observe connected peers
    socket.on('PeerListChanged', function (data) {
        console.log('The peer list has changed...');
        // Reset the PeerList model so view can refresh
        app.peerList.fetch({
            reset: true
        });
    });
};

$(document).ready(function () {
    console.log('Initializing the local player...');
    app.localPlayer = new LocalPlayer();

    console.log('Initializing the peer list...');
    app.peerList = new PeerList();

    /* 
          Holds the peer locally selected. These are 
          the ones who will be notified by 'startPlaying' when
          the user hit 'play' or 'queue'.
    */
    app.selectedPeers = [];

    console.log('Initializing the left menu...');
    app.leftMenuView = new LeftMenuView();

    console.log('Connecting to server via socket.io...');
    connectToServer();

    // Is the request coming from the RPI ?
    //var isRPI = 


});