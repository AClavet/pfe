// Add a close method to all views
Backbone.View.prototype.close = function() {
    // Empty the view HTML
    $('#MainPanelView').empty();

    // Remove all events
    this.undelegateEvents();

    // Handle on close event
    if (this.onClose) {
        this.onClose();
    }
}

/*
    The local player. This class holds the local playlist
    and is used to control music playback on this peer.
*/
function LocalPlayer() {

    var playlist = new Playlist();

    // Availaible players
    var audio_player = $("#audioPlayer")[0];
    var video_player = $("#videoPlayer")[0];

    // Which player is actually in use ?
    var current_player = null;

    var sourceBase = 'http://' + location.host + '/media/';
    var playlist_curr_idx = 0;

    var playBtn = $('#playBtn');
    var fwdBtn = $('#fwdBtn');
    var bwdBtn = $('#bwdBtn');
    var pauseBtn = $('#pauseBtn');
    var trackLabel = $('#trackLabel');

    var stateChanged = new Event('stateChanged');

    registerEvents = function() {
        if (audio_player) {
            audio_player.addEventListener('ended', movePlaylistForward);
            audio_player.addEventListener('playing', showPauseBtn);
        } else {
            console.log('HTML5 audio player not found.')
        }

        if (video_player) {
            video_player.addEventListener('ended', movePlaylistForward);
            video_player.addEventListener('playing', showPauseBtn);
        } else {
            console.log('HTML5 video player not found.')
        }

        playBtn.click(togglePlayState);
        pauseBtn.click(togglePlayState);
        fwdBtn.click(movePlaylistForward);
        bwdBtn.click(movePlaylistBackward);

        refreshUI();
    }

    var refreshUI = function() {
        var hasPreviousTrack = playlist_curr_idx > 0;
        if (!hasPreviousTrack) {
            bwdBtn.addClass('disabled');
        } else {
            bwdBtn.removeClass('disabled');
        }

        var hasNextTrack = playlist.length > 0 && playlist_curr_idx < playlist.length - 1;
        if (!hasNextTrack) {
            fwdBtn.addClass('disabled');
        } else {
            fwdBtn.removeClass('disabled');
        }

        var hasPlaylist = playlist.length > 0;
        if (hasPlaylist) {
            playBtn.removeClass('disabled');
            pauseBtn.removeClass('disabled');
        } else {
            playBtn.addClass('disabled');
            pauseBtn.addClass('disabled');
        }

        if (hasPlaylist) {
            current_item = playlist[playlist_curr_idx];
            if (current_item) {
                trackLabel.text(current_item.replace(/^.*[\\\/]/, ''));
            } else {
                trackLabel.text('');
            }
        }
    }

    this.getPlaylist = function() {
        return playlist;
    }

    var movePlaylistBackward = function() {
        var request = new PlayRequest("previous", app.peerList, playlist);
        self.socket.emit('PlayRequest', JSON.stringify(request));
    }

    var movePlaylistForward = function() {
        var request = new PlayRequest("next", app.peerList, playlist);
        self.socket.emit('PlayRequest', JSON.stringify(request));
    }

    this.resume = function(broadcast) {
        var request = new PlayRequest("play", app.peerList, playlist);
        self.socket.emit('PlayRequest', JSON.stringify(request));
    }

    this.playlistAdd = function(file) {
        var request = new PlayRequest("playlist_add", app.peerList, playlist);
        self.socket.emit('PlayRequest', JSON.stringify(request));
    }

    this.playlistAddAll = function(arrayOfFile) {
        console.log("Setting playlist: " + arrayOfFile);
        // playlist = arrayOfFile;
        // TODO
    }

    this.playlistSet = function(arrayOfFile) {
        console.log("Setting playlist: " + arrayOfFile);
        playlist = arrayOfFile;
        playlist_curr_idx = 0;
    }

    this.playlistClear = function() {
        playlist = [];
        playlist_curr_idx = 0;
        refreshUI();
    }

    this.play = function(trackUrl) {
        if (!trackUrl) {
            trackUrl = playlist[playlist_curr_idx];
        }
        if (!trackUrl) {
            console.log('Playlist is empty or no argument provided');
            return;
        }

        if (current_player) {
            current_player.setAttribute('src', '');
        }

        var fullUrl = sourceBase + '?file=' + encodeURI(trackUrl.replace(/\//g, '\\'));
        console.log('Starting playback of: ' + fullUrl);

        var ext = fullUrl.substr(fullUrl.lastIndexOf('.') + 1);

        if (video_player.canPlayType("video/" + ext) !== "") {
            console.log('Interpreted file as video...');
            video_player.setAttribute('src', fullUrl);
            video_player.play();
            current_player = video_player;
            $('#videoPlayer').removeClass('hidden');
        } else {
            if (audio_player.canPlayType("audio/" + ext)) {
                console.log('Interpreted file as audio...');
                audio_player.setAttribute('src', fullUrl);
                audio_player.play();
                current_player = audio_player;
                $('#videoPlayer').addClass('hidden');
            } else {
                console.log('The specified file format is not supported: ' + ext);
            }
        }
        refreshUI();
    }

    this.pause = function() {
        var request = new PlayRequest("pause", app.peerList, playlist);
        self.socket.emit('PlayRequest', request);
    }

    this.resume = function() {
        var request = new PlayRequest("resume", app.peerList, playlist);
        self.socket.emit('PlayRequest', request);
    }

    this.skipForward = function() {
        var request = new PlayRequest("next", app.peerList, playlist);
        self.socket.emit('PlayRequest', JSON.stringify(request));
    }

    this.skipBackward = function() {
        var request = new PlayRequest("previous", app.peerList, playlist);
        self.socket.emit('PlayRequest', request);
    }

    this.handlePlayRequest = function(request) {
        console.log("Handling action: " + request.action);

        switch (request.action) {
            case "play":
                app.localPlayer.playlistSet(request.playlist);
                app.localPlayer.play();
                break;

            case "stop":
                current_player.pause();
                break;

            case "pause":
                current_player.pause();
                playBtn.removeClass('hide');
                pauseBtn.addClass('hide');
                break;

            case "resume":
                current_player.play();
                playBtn.addClass('hide');
                pauseBtn.removeClass('hide');
                break;

            case "next":
                console.log('Moving playlist forward...');
                if (playlist_curr_idx < playlist.length) {
                    playlist_curr_idx++;
                    var nextTrack = playlist[playlist_curr_idx];
                    app.localPlayer.play(nextTrack);
                } else {
                    console.log("Playlist is empty.")
                }
                break;

            case "previous":
                console.log('Moving playlist backward...');
                if (playlist_curr_idx > 0) {
                    playlist_curr_idx--;
                    var previousTrack = playlist[playlist_curr_idx];
                    app.localPlayer.play(previousTrack);
                } else {
                    console.log("Playlist is empty.")
                }
                break;

            case "playlist_clear":
                app.localPlayer.playlistClear();
                break;

            case "playlist_add":
                app.localPlayer.playlistAddAll(request.playlist);
                break;

            default:
                console.log('Could not handle the PlayRequest: ' + JSON.stringify(request));
                break;
        }
        refreshUI();
    }

    var showPauseBtn = function() {
        playBtn.addClass('hide');
        pauseBtn.removeClass('hide');
        refreshUI();
    }

    var getPlayBtnEnabled = function() {
        if (current_player) {
            return playlist.length > 0 || current_player.duration > 0;
        } else {
            return false;
        }
    }

    var togglePlayState = function() {
        if (current_player && !current_player.paused) {
            var request = new PlayRequest("pause", app.peerList, playlist);
            self.socket.emit('PlayRequest', JSON.stringify(request));

        } else { // Not playing
            var request = new PlayRequest("resume", app.peerList, playlist);
            self.socket.emit('PlayRequest', JSON.stringify(request));
        }
        refreshUI();
    }

    registerEvents();
}

/*
    The left panel with the navigation menus.
*/
var LeftMenuView = Backbone.View.extend({
    el: "#left_menu_target",
    template: $("#LeftMenuView"),

    initialize: function() {
        this.CURRENT_VIEW = null;
        this.render();
        this.showLibrary();
    },

    render: function() {
        console.log('Rendering the menu...');

        var tmpl = _.template(this.template.html());
        this.$el.html(tmpl());
        return this;
    },

    events: {
        "click .libraryBtn": "showLibrary",
        "click .peersBtn": "showPeers",
        "click .historyBtn": "showHistory",
        "click .playlistBtn": "showPlaylist"
    },

    showLibrary: function() {
        console.log('Showing the library view...');
        $('a').removeClass('active');
        $('.libraryBtn').addClass('active');
        this.removeCurrentView();
        this.CURRENT_VIEW = new LibraryView();
    },

    showPeers: function() {
        $('a').removeClass('active');
        $('.peersBtn').addClass('active');
        this.removeCurrentView();
        this.CURRENT_VIEW = new PeersView();
    },

    showHistory: function() {
        $('a').removeClass('active');
        $('.historyBtn').addClass('active');
        this.removeCurrentView();
        this.CURRENT_VIEW = new HistoryView();
    },

    showPlaylist: function() {
        $('a').removeClass('active');
        $('.playlistBtn').addClass('active');
        this.removeCurrentView();
        this.CURRENT_VIEW = new PlaylistView();
    },

    removeCurrentView: function() {
        if (this.CURRENT_VIEW) {
            this.CURRENT_VIEW.close();
        }
    }
});

/*
    This view shows a list of ContentSources. It's
    used by a peer to browse files on the server.
*/
var ContentSourceView = Backbone.View.extend({
    tagName: "a",

    className: "list-group-item subitem collapse",

    template: $("#contentSourceTemplate").html(),

    render: function() {
        var tmpl = _.template(this.template);
        this.$el.html(tmpl(this.model.toJSON()));
        return this;
    },

    events: {
        "click .contentSourceTemplateEntry": "drillDownContentSource",
    },

    // Show a treeview structure of a content source
    // TODO: Find a more efficient way to display. The plugin is unreliable and slow..
    drillDownContentSource: function() {
        var entryTemplate = $("#ContentEntryTemplate").html();

        var previousTree = $('#tree');
        if (previousTree) {
            previousTree.remove();
        }

        var self = this;

        jQuery.ajax({
            type: "GET",
            url: 'library/' + this.model.id,
            beforeSend: function() {
                $('#loading-indicator').show();
            },
            success: function(data) {
                self.$el.append("<div id='tree'></div>");
                $("#tree").bind('loaded.jstree', function(e, data) {
                    $('#loading-indicator').hide();
                })
                    .jstree({
                        "json_data": {
                            "data": data
                        },
                        'types': {
                            'valid_children': ['folder'],
                            'types': {
                                'folder': {
                                    'valid_children': ['file'],
                                    'max_depth': 1,
                                    'icon': {
                                        "image": ""
                                    }
                                },

                                'file': {
                                    'valid_children': ['none'],
                                    'icon': {
                                        "image": "img/file.png"
                                    }
                                }
                            }
                        },
                        "themes": {
                            "theme": "classic",
                            "dots": false,
                            "icons": true
                        },
                        "plugins": ["themes", "json_data", "ui", "types", "checkbox"]
                    });
            },
            error: function(data) {
                $('#loading-indicator').hide();
            }
        });
    }
});

/*
    This is the Library page.

    It shows:


*/
var LibraryView = Backbone.View.extend({

    el: $("#MainPanelView"),

    template: $("#LibraryViewTemplate").html(),

    initialize: function() {
        this.collection = new Library();
        this.collection.fetch({
            reset: true
        });
        this.collection.bind('reset', this.render, this);
    },

    onClose: function() {
        this.collection.unbind("reset", this.render);
    },

    render: function() {
        var tmpl = _.template(this.template);
        this.$el.append(tmpl());

        var that = this;
        _.each(this.collection.models, function(item) {
            that.renderSource(item);
        }, this);
    },

    renderSource: function(item) {
        var contentSourceView = new ContentSourceView({
            model: item
        });
        this.$('#contentSources').append(contentSourceView.render().el);
    },

    events: {
        "click #btnPlay": "playSelectedFiles",
        "click #btnQueue": "queueSelectedFiles",
    },

    playSelectedFiles: function() {
        var playlist = [];

        $(".jstree-checked").each(function(i, element) {
            var file = $(element).data()["fullFilePath"];
            playlist.push(file);
        });

        var request = new PlayRequest("play", app.selectedPeers, playlist);
        self.socket.emit("PlayRequest", JSON.stringify(request));
    },

    queueSelectedFiles: function() {
        var playlist = [];

        $(".jstree-checked").each(function(i, element) {
            var file = $(element).data()["fullFilePath"];
            playlist.push(file);
        });

        var request = new PlayRequest("playlist_add", app.selectedPeers, playlist);
        self.socket.emit("PlayRequest", request);
    }
});

/*
    This view is used to browse and select all the peers
    connected to the server.
*/
var PeersView = Backbone.View.extend({

    el: $("#MainPanelView"),
    template: $("#PeersTemplate").html(),

    initialize: function() {
        this.collection = app.peerList;
        this.collection.bind('add', this.renderTable, this);
        this.collection.bind('remove', this.renderTable, this);
        this.collection.bind('reset', this.render, this);
        this.collection.fetch({
            reset: true
        });
    },

    onClose: function() {
        this.collection.unbind("add", this.renderTable);
        this.collection.unbind("remove", this.renderTable);
        this.collection.unbind("reset", this.render);
    },

    events: {
        "change #cbAllPeers": "selectAllPeers",
        "change .selectPeer": "selectPeer"
    },

    selectAllPeers: function() {
        console.log('all peers');
        app.selectedPeers = [];
        var that = this;
        _.each(self.peerList.models, function(peer) {
            that.selectedPeers.push(peer);
        }, this);
    },

    selectPeer: function() {
        console.log('select one peer');
    },

    render: function() {
        // Empty the view
        $("#MainPanelView").empty();
        // Render the base template with the table and header 
        console.log('Rendering PeerView...');
        var tmpl = _.template(this.template);
        $("#MainPanelView").append(tmpl());
        console.log('Rendering peers rows...');

        // Render each peer
        this.renderTable();
    },

    renderTable: function() {
        var peerTable = this.$('#peerTable');
        if (peerTable) {
            peerTable.find("tr:gt(0)").remove();
            var that = this;
            that.unique_peers = [];
            _.each(this.collection.models, function(item) {
                var jsonItem = item.toJSON();
                if (that.unique_peers.indexOf(jsonItem.URL) === -1) {
                    that.renderPeerRow(item);
                    that.unique_peers.push(jsonItem.URL);
                }
                else {
                    that.unique_peers.push(jsonItem.URL);
                }
            });
        }
    },

    renderPeerRow: function(item) {
        var rowTmpl = $('#PeerRowTemplate').html();
        var tmpl = _.template(rowTmpl);

        this.$('#peerTable > tbody:last').append(tmpl(item.toJSON()));
    }
});

var PlaylistView = Backbone.View.extend({

    el: $("#MainPanelView"),

    template: $("#PlaylistViewTemplate").html(),

    initialize: function() {
        this.collection = app.localPlayer.getPlaylist();
        this.render();
    },

    render: function() {
        $("#MainPanelView").empty();
        console.log('Rendering PlaylistView...');
        var tmpl = _.template(this.template);
        $("#MainPanelView").append(tmpl());
        this.renderTable();
    },

    renderTable: function() {
        var table = $('#playlistTable');
        if (table) {
            table.find("tr:gt(0)").remove();
            var that = this;
            this.collection = app.localPlayer.getPlaylist();
            console.log(this.collection);
            _.each(this.collection, function(item) {
                console.log('rending playlist row...');
                var short_filename = item.replace(/^.*[\\\/]/, '');
                that.renderRow({item_name:short_filename});
            }, this);
        } else {
            console.log('Could not find the playlist table.');
        }
    },

    renderRow: function(item) {
        var rowTmpl = $('#PlaylistRowTemplate').html();
        var tmpl = _.template(rowTmpl);

        this.$('#playlistTable > tbody:last').append(tmpl(item));
    }
});