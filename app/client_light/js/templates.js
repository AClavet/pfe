< script id = "contentSourceTemplate"
type = "text/x-underscore-template" >
<!--
< span
class = "glyphicon glyphicon-chevron-right" > < /span> &nbsp 
      -->
   
         <a href="#" class="contentSourceTemplateEntry" data-toggle="collapse" data-target=".sourceTree_<%= id %>"><%= displayName %>
         </a >

< div class = "sourceTree_<%= id %> collapse" > < /div>
   
       
</script >

< script id = "LeftMenuView"
type = "text/x-underscore-template" > < ul class = "sidebar-nav" > < li class = "sidebar-brand" > < a href = "#" > Media center < /a></li > < li > < a href = "#"
class = "active libraryBtn" > Library < /a></li > < li > < a href = "#"
class = "peersBtn" > Peers < /a></li > < li > < a href = "#"
class = "playlistBtn" > Playlists < /a></li > < li > < a href = "#"
class = "historyBtn" > History < /a></li > < /ul>
</script >

< script id = "LibraryViewTemplate"
type = "text/x-underscore-template" > < div class = "content-header" > < h1 >
  Library < button type = "button"
class = "btn btn-sm btn-primary" > Add content < /button>
          
        
       <button type="button" class="btn btn-default" id="btnQueue">Queue</button >

< img src = "ext/themes/classic/throbber.gif"
id = "loading-indicator"
style = "display:none" / >

< /h1>
     </div >
<!-- Page content -->
< div class = "page-content inset" > < div class = "list-group" > < div id = "contentSources" > < /div>
         </div > < /div> 
</script >

< script id = "PeerRowTemplate"
type = "text/x-underscore-template" > < tr > < td > < input type = "checkbox"
class = "selectPeer" > < /td>
         <td><%= URL %> </td > < td > <%
if (ThisPeer) { %> < span class = "label label-info label" > It & lsquo;
  s you < /span>
           <% } %>
           </td > < /tr>
</script >

  < script id = "PeersTemplate"
  type = "text/x-underscore-template" > < div class = "content-header" > < h1 >

  Peers


  < button type = "button"
  class = "btn btn-sm btn-primary"
  id = "RefreshPeers" > Refresh < /button>
   
     <img src="ext/themes / classic / throbber.gif " id="
  loading - indicator " style="
  display: none " />
   
     </h1>
   </div>
   <!-- Page content -->
    <div class="
  page - content inset ">
       <table class="
  table table - striped table - nonfluid " id="
  peerTable ">
         <thead>
           <tr>
             <th> <label>
               <input type="
  checkbox " id='cbAllPeers'>Select all</input>
             </label></th>
           </tr>
         </thead>
         <tbody>
           <!--  Peer rows will go here -->
         </tbody>
       </table>
  
   </div> 
</script>