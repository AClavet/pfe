/*
    A peer is a client that can be controlled remotely to
    play music or videos. When you are browsing the system HTML
    pages, you are a peer.
*/
var Peer = Backbone.Model.extend({
    defaults: {
        URL: "",
        State: "",
        Selected: false,

        ThisPeer: false,

        id: function() {
            return URL;
        }
    },
});

var PeerList = Backbone.Collection.extend({
    model: Peer,
    url: "peers"
});

var track = Backbone.Model.extend({
    defaults: {
        filePath: ""
    }
});

var Playlist = Backbone.Collection.extend({
    model: track
});

/*

  A content source is a folder made acessible by the server.

  It's from a content source that music and videos files 
  are read and selected by peers.

*/
var ContentSource = Backbone.Model.extend({
    defaults: {
        id: "",
        fullPath: "",
        displayName: "[Undefined]",
    },

    url: function() {
        return "library/" + this.id;
    }
});

// The library is a server list of content sources
var Library = Backbone.Collection.extend({
    model: ContentSource,
    url: "library"
});

var LibraryEntry = Backbone.Model.extend({
    defaults: {
        path: "",
        type: ""
    },
});

/**

    Playrequest are sent to 

*/

function PlayRequest(action, targets, playlist) {

    // One of: 
    // ["play", "stop", "pause", "next", "previous", "resume", "seek", "playlist_clear", "playlist_add"];
    this.action = action;
    
    // Which peer(s) should execute this play request
    this.targets = targets || [];

    // This peer current playlist
    this.playlist = playlist || [];

    // The time at which the request was emitted
    this.request_time = (new Date());

    // The sender id will be set by the server upon reception
    this.sender_id = '';

}