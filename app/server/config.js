/*
	Configuration module.

	You can load it like this:

	var config = require('./config')

*/

var config = {}

config.vlc = {};
config.http_server = {};

config.log_full_name = "./app.log";
config.vlc.stream_port = 8080;
config.http_server.port = 80;
config.app_data_folder = "./app_data/"

module.exports = config;