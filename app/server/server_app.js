/*

	This is the application main class. It's job is
	to start annd stop the application main services, namely:

		- The HTTP server; 
		- The VLC streaming server;
		- The content service;
		- The peer manager.

	The app will read the config.js file to configure these services.

	You can override the config.js values when starting the application
	by providing command line arguments.

	See --help for more info (TODO).
*/

var HttpServer = require('./http_server.js');
var ContentService = require('./content_service.js');
var config = require('./config');
var DiscoveryService = require('./discovery_service.js');

function app(httpServerPort, vlcStreamPort, appDataFolder) {
	var self = this;

	var httpServer = null;

	var contentService = null;

	var discovery = null;

	this.start = function() {
		console.log("\nStarting application services...");
		initServices();
		startServices();
	}

	this.stop = function() {
		console.log("\nStopping application services...");
		stopServices();
	}

	var initServices = function() {
		httpServer = new HttpServer(httpServerPort || config.http_server.port);
		contentService = new ContentService(appDataFolder || config.app_data_folder);
		discovery = new DiscoveryService();
	}

	var startServices = function() {
		httpServer.start();
		discovery.start();
	}

	var stopServices = function() {
		if (httpServer) {
			httpServer.stop();
		}
		if (discovery) {
			discovery.stop();
		}
	}

	// Bind to EXIT event to be sure to close services gracefully
	process.on('SIGINT', function() {
		self.stop();
	});
}

app = new app();

try {
	app.start();
} catch (ex) {
	console.log('An unhandled error has occured: ' + ex);
	app.stop();
}