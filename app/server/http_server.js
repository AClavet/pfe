var express = require('express');
var fs = require('fs');
var path = require('path');
var socket = require('socket.io');

function HttpServer(port) {
	var self = this;

	var app = express();
	app.use(express.bodyParser());

	var server = require('http').createServer(app);
	var io = null;

	var port = port || 80;

	this.clients = [];

	this.start = function() {
		console.log('Starting the HttpServer on port: ' + port);
		server.listen(80);
		io = socket.listen(server);
		io.set('log level', 1);
		registerEvents(app);
	}

	this.stop = function() {
		if (app) {
			console.log('Stopping the HttpServer');
			server.close();
		}
	}

	var requestCallback = function(res, err, result) {
		if (err) {
			console.log(err);
		} else {
			res.send(result);
		}
	}

	var registerEvents = function(_app) {
		console.log('Registering HttpServer routes...');

		var mainFilePath = path.dirname(require.main.filename);

		// Index
		_app.use(express.static(path.join(mainFilePath, '../client_light')));

		// Error handling
		_app.use(function(err, req, res, next) {
			console.error(err.stack);
			res.send(500, 'An error occured while handling the request!');
		});

		_app.get("/media", function(req, res) {
			// Get fullfilepath from the request
			var filePath = req.query['file'].replace(' ', '\ ');
			console.log("Serving file: " + filePath + " to: " + req.connection.remoteAddress);
			try {
				res.sendfile(filePath);
			} catch (ex) {
				console.log(ex);
				res.send('');
			}
		});

		_app.get("/library", function(req, res) {
			process.emit('getLibrary', req.body, function(err, result) {
				requestCallback(res, err, result);
			});
		});

		_app.get("/library/:id", function(req, res) {
			process.emit('getLibraryTree', req.param('id'), function(err, result) {
				requestCallback(res, err, result);
			});
		});

		_app.get("/peers", function(req, res) {
			var clients = [];
			io.sockets.clients().forEach(function(socket) {
				var endpoint = socket.manager.handshaken[socket.id].address;

				clients.push({
					URL: endpoint.address
				});
			});
			requestCallback(res, null, JSON.stringify(clients));
		});

		_app.post("/library", function(req, res) {
			process.emit('saveLibrary', req.body, function(err, result) {
				requestCallback(res, err, result);
			});
		});

		_app.post("/player", function(req, res) {
			var playRequest = req.body;
			process.emit('playRequest', playRequest, function(err, result) {
				requestCallback(res, err, result);
			});
		});

		io.sockets.on('connection', function(client) {
			var clientAddress = client.handshake.address.address;
			console.log('A client has connected from ' + clientAddress);
			io.sockets.emit('PeerListChanged', clientAddress);

			client.on('disconnect', function() {
				io.sockets.emit('PeerListChanged');
			});

			client.on('PlayRequest', function(request) {
				console.log('Broadcasting PlayRequest...');
				if (request) {
					request.sender_id = client.handshake.address.address;
					io.sockets.emit('PlayRequest', request);
				} else {
					console.log('Received empty request');
				}
			});
		});
	}
}

module.exports = HttpServer;