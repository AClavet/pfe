/*
	When started, this module will
	listen for UDP requests on the specified port.

	The request should come from a Raspberry Pi service.

	When a DiscoveryRequest is received, this module will
	answer with a DiscoveryReply.
*/
var polo = require('polo');
var dgram = require('dgram');

function DiscoveryService(listen_port) {
	var self = this;

	var PORT = listen_port || 5000;

	var apps = polo();

	apps.put({
		name: 'MediacenterService',
		port: PORT
	});

	this.start = function() {

	}

	this.stop = function() {
		
	}
}

module.exports = DiscoveryService;