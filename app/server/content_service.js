var fs = require("fs");
var path = path = require('path');

function ContentService(appDataFolder) {
	var self = this;

	this.DATA_SOURCES_FILE_NAME = "data_sources.json"

	this.DATA_SOURCE_FOLDER = appDataFolder;

	this.getSources = function() {
		if (createSourceFileIfNotExists()) {
			return fs.readFileSync(getContentSourcesFileName(),'UTF-8');
		} else {
			return null;
		}
	}


	var registerEvents = function() {
		process.on('getLibrary', handleOnGetLibrary);
		process.on('getLibraryTree', handleOnGetLibraryTree);
		process.on('saveLibrary', handleOnSaveLibrary);
	}

	var handleOnGetLibrary = function(data, callback) {
		return callback(null, self.getSources());
	}

	var handleOnGetLibraryTree = function(libraryID, callback) {
		var jsonSources = fs.readFileSync(getContentSourcesFileName(), 'UTF-8');
		var sources = JSON.parse(jsonSources);

		sources.forEach(function(source) { 
			if (source && source.id === libraryID) {
				return callback(null, self.dirTree(source.fullPath));
			}
		});
	}


	// This function is used to scan a directory recursively and return the representing JSON structure
	this.dirTree = function(filename) {
	    var stats = fs.lstatSync(filename),
	        info = {
	            name: filename,
	            data: path.basename(filename),
	            metadata: {
			     "fullFilePath": filename
			  }
	        };

	    if (stats.isDirectory()) {
	        info.attr= {"rel":"folder"};
	        info.children = fs.readdirSync(filename).map(function(child) {
	            return self.dirTree(filename + '/' + child);
	        });
	    } else {
	        // Assuming it's a file. In real life it could be a symlink or
	        // something else!
	        info.attr= {"rel":"file"};
	        info.rel = "file";
	    }

	    return info;
	}

	var handleOnSaveLibrary = function(data, callback) {
		var contentSourcesFile = path.join(appDataFolder, DATA_SOURCES_FILE_NAME);	
		fs.writeFile(contentSourcesFile, JSON.stringify(data, null, 4), function(err) {
	    	if (err) {
	      		callback(err);
	    	} else {
	      		callback(null, self.getSources());
	    	}
		}); 
	}


	var createSourceFileIfNotExists = function() {
		var contentSourcesFile = getContentSourcesFileName();
		var exists = fs.existsSync(contentSourcesFile);
		if (!exists) {
			fs.writeFileSync(contentSourcesFile, null);
		}
		return fs.existsSync(contentSourcesFile);
	}

	var getContentSourcesFileName = function() {
		return path.join(self.DATA_SOURCE_FOLDER,  self.DATA_SOURCES_FILE_NAME);
	}

	registerEvents();
}


module.exports = ContentService;