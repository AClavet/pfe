var path = require('path');
var fs = require('fs');
var os = require('os');
var omx = require('omxcontrol');
var child_process = require('child_process');

/*

    This player is used to control an omxplayer process.

    When the omxplayer process stops playing, omx.onstop should be
    called (TODO: VERIFIER);

*/
function OmxPlayer() {
    var self = this;

    this.playlist = [];

    this.playlist_curr_idx = 0;

    this.play = function() {
        console.log('Starting to play the current playlist item...');
        // Is there a file to play ?
        if (this.playlist.length >= 0 && this.playlist_curr_idx <= this.playlist.length) {
            var currentPlaylistItem = this.playlist[this.playlist_curr_idx];

            // Kill the current omxplayer process if it exists
            this.kill_omx_process(function() {

                // Start playing the file
                console.log('Starting playback of: ' + currentPlaylistItem);
                omx.start(currentPlaylistItem, function() {
                    console.log('Started playback of:' + currentPlaylistItem);
                });
            });
        } else {
            console.log('The playlist is empty. Going idle.');
        }
    }

    this.kill_omx_process = function(callback) {
        console.log('Killing omxplayer.bin process if it exists...');
        var cmd = 'pkill omxplayer.bin';

        var process = child_process.exec(cmd, function(error, stdout, stderr) {
            if (error !== null) {
                console.log(error);
            }
            if (callback) {
                callback();
            }
        });
    }

    this.playist_set = function(playlist) {
        this.playlist = playlist;
        this.playlist_curr_idx = 0;
    }

    this.playist_add = function(playlist) {
        this.playlist.push(playlist);
    }

    this.playist_clear = function() {
        this.playlist = [];
        this.playlist_curr_idx = 0;
    }

    this.next = function() {
        if (this.playlist_curr_idx < this.playlist.length) {
            this.playlist_curr_idx++;
            this.play();
        } else {
            console.log("Playlist is empty.");
        }
    }

    this.previous = function() {
        if (this.playlist_curr_idx > 0) {
            this.playlist_curr_idx--;
            this.play();
        } else {
            console.log("Playlist is empty.")
        }
    }

    this.pause = function() {
        console.log('Sending pause CMD to omx...');
        omx.pause();
    }

    this.resume = function() {
        console.log('Sending resume CMD to omx...');
        omx.pause();
    }

    this.handle_play_request = function(request, server_url) {
        console.log("Handling request: " + request.action);

        var playlist = [];
        request.playlist.forEach(function(track_url) {
            var requestString = 'http://' + server_url + '/media/?file=' + encodeURIComponent(track_url);
            playlist.push(requestString);
        });

        switch (request.action) {

            case "play":
                this.playist_set(playlist);
                this.play();
                break;

            case "stop":
                this.pause();
                break;

            case "pause":
                this.pause();
                break;

            case "resume":
                this.resume();
                break;

            case "next":
                console.log('Moving playlist forward...');
                this.next();
                break;

            case "previous":
                this.previous();
                break;

            case "playlist_clear":
                this.playlist_clear(playlist);
                break;

            case "playlist_add":
                this.playlist_add(playlist);
                break;

            default:
                console.log('Could not handle the request: ' + request);
                break;
        }
    }

    omx.onstop = function(wasKilled, stdout) {
        console.log("[omxplayer] stopped playback by itself");
    }

    this.kill = function() {
        this.kill_omx_process();
        omx.quit();
    }
}

module.exports = OmxPlayer;