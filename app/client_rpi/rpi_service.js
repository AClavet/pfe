/*
	RpiApp is an application made to run in the background
	on a Raspberry. While initializing, the RpiApp will try 
	to discover a server. 

	Once a server is found, the RpiApp can answer
	to PlayRequests received from others peers.

	PlayRequests are handled by the OmxPlayerProcess. This
	process is used to control the omxplayer software

	usage: nodje rpi_service.js

*/

var io = require("socket.io-client");
var polo = require('polo');
var omx = require('./omx.js');

function RpiApp() {
	var self = this;

	// A socket.io connection to the server.
	// Will be null if no server is known.
	var socket = null;

	// Omxplayer
	self.player = new omx();

	// A known server if one was found
	self.server_url = '';

	// Start the rpi service
	this.start = function() {
		console.log("\nStarting the RPI service...");

		// Initialize polo module for server discovery
		var apps = polo();

		// up fires everytime a server comes up
		apps.once('up', function(name, service) {
			console.log('Server detected. Attempting to connect...');
			self.server_url = service.host;
			self.bind_to_server(service.host);
		});
	}

	// Stop the rpi service
	this.stop = function() {
		console.log("Stopping application services...");
	}

	// Bind to a server to receive PlayRequests
	this.bind_to_server = function(server_url) {
		if (socket) {
			socket.close();
		}

		console.log('Connecting to server: ' + server_url);
		socket = io.connect(server_url);

		// Log sucessfull server connection attemps
		socket.on('connect', function() {
			console.log('Connected to: ' + server_url);
		});

		// Bind on 'disconnect' to reset the server_url and
		// to log the event.
		socket.on('disconnect', function() {
			console.log('Disconnected from server: ' + server_url);
		});

		// PlayRequests are received from other peers by the server
		socket.on('PlayRequest', function(request) {

			// Delegate request handling to the player
			self.player.handle_play_request(JSON.parse(request), self.server_url);
		});
	}

	// Bind to EXIT event to be sure to close services gracefully
	process.on('SIGINT', function() {
		self.stop();
		self.player.kill();
		process.exit(0);
	});
}

try {
	var app = new RpiApp();
	app.start();
} catch (ex) {
	console.log('An unhandled error has occured:' + ex);
	app.stop();
}