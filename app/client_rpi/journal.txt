

#Installing the latest debian image on a disk
1. Download the latest Raspberry Pi image (Raspbian).
2. Insert the target disk into your computer.
3. On Windows, use Win32DiskManger to burn the image onto the disk.
4. Once the disk is ready, insert it into the Raspberry PI

#Transform the image into a kiosk
1. Log in using ssh : pi@address.com , password = raspberry
2. ssh pi@192.168.1.148
3. Create user kiosk
	sudo adduser kiosk, password gogo
4. sudo apt-get install matchbox-window-manager -y
5. sudo apt-get update
6. sudo apt-get install chromium-browser -y