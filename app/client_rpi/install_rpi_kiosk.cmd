#!/bin/sh
#
# install_rpi_kiosk
#
# Purpose
# This script will install ... 

# After running this script, your system will have:

# 1. A new user called kiosk with password goto;
# 2. lightdm as a display manager;
# 3. lightdm booting silenty at startup with user kiosk;
# 4. chromium-broswer running in fullscreen mode.
# 5. A Server discovery service that will look for 
#    a server periodically when none is known.
# 6. openssh-server
# 7. matchbox-window-manager as the window manager

# Image used
#TODO : VERSIONS

# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

apt-get update

# Add user kiosk
useradd kiosk -p goto

# Copy user home in /opt/ with read-only access
mkdir /home/kiosk
mkdir /opt/kiosk
cp -r /home/kiosk /opt/

cd /opt/kiosk/
chmod -R a+r .
touch .xsession
chmod a+x .xsession

# Install rsync
sudo apt-get install rsync -y

# Write .xsession boot script to kiosk home dir
/bin/cat << EOF
xset s off
xset -dpms
matchbox-window-manager &
while true; do
	 rsync -qr --delete --exclude='.Xauthority' /opt/kiosk/ $HOME/
	# TODO: Remove server static ip for 'unknown'
    chromium-browser --app=http://192.168.1.149/
done
EOF > /opt/kiosk/.xsession


# Install git
apt-get install git -y

# Install chromium-browser
apt-get install chromium-broswer -y

# Install the windows manager
apt-get install matchbox-window-manager -y

# Install openssh
apt-get install openssh-server -y

# Configure lightdm so it boots with user kiosk
'default-user=kiosk' >>  /etc/lightdm/lightdm.conf




